const template = `{{#if compareUrl}}
# [v{{nextRelease.version}}]({{compareUrl}}) ({{datetime "UTC:yyyy-mm-dd"}})
{{else}}
# v{{nextRelease.version}} ({{datetime "UTC:yyyy-mm-dd"}})
{{/if}}

{{#with commits}}
{{#if major}}
## 💥 Breaking Changes
{{#each major}}{{> commitTemplate}}{{/each}}
{{/if}}

{{#if minor}}
## ✨ New Features
{{#each minor}}{{> commitTemplate}}{{/each}}
{{/if}}

{{#if patch}}
## 🐛 Fixes
{{#each patch}}{{> commitTemplate}}{{/each}}
{{/if}}

{{#if other}}
## 🥅 Other
{{#each other}}{{> commitTemplate}}{{/each}}
{{/if}}

{{/with}}`;

const commitTemplate = `{{#if issues}}- {{gitmoji}} {{subject}} {{author.name}} (Commit: {{commit.short}}, Issues:{{#each issues}} [\`{{text}}\`]({{link}}){{/each}})
{{/if}}`;

module.exports = {
  branches: ['main'],
  plugins: [
    [
      'semantic-release-gitmoji',
      {
        releaseNotes: {
          semver: true,
          template,
          partials: {
            commitTemplate
          }
        }
      }
    ],
    '@semantic-release/gitlab'
  ]
};
