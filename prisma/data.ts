import { Prisma } from '@prisma/client';

export const users: Prisma.UserCreateInput[] = [
  {
    email: 'robin@louarn.fr',
    password: 'robin@louarn.fr',
    firstname: 'Robin',
    lastname: 'Louarn'
  }
];

export const cityStades: Prisma.CityStadeCreateInput[] = [
  {
    id: 'kerinou',
    name: 'City stade de kerinou',
    address: '23 Rue Notre Dame de Bonne Nouvelle, 29200 Brest'
  }
];
