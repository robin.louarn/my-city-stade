import { db } from '@/db/prisma';
import { hash } from 'bcrypt';
import { cityStades, users } from './data';

const seedUsers = async () => {
  await Promise.all(
    users.map(async ({ email, password, ...inputs }) =>
      db.user.upsert({
        where: {
          email
        },
        create: {
          email,
          password: await hash(password, 10),
          ...inputs
        },
        update: {}
      })
    )
  );
};

const seedCityStades = async () => {
  await Promise.all(
    cityStades.map(async ({ id, ...inputs }) =>
      db.cityStade.upsert({
        where: {
          id
        },
        create: {
          id,
          ...inputs
        },
        update: {}
      })
    )
  );
};

const seed = async () => {
  await seedUsers();
  await seedCityStades();
};

seed();
