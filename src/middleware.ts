import { defaultLocale, locales } from '@/constants/intl';
import { NextRequestWithAuth, withAuth } from 'next-auth/middleware';
import createIntlMiddleware from 'next-intl/middleware';
import { NextFetchEvent } from 'next/server';

const publicPages = ['/', '/login', '/register'];

const intlMiddleware = createIntlMiddleware({
  locales,
  defaultLocale
});

const authMiddleware = withAuth((req) => intlMiddleware(req), {
  callbacks: {
    authorized: ({ token }) => token != null
  },
  pages: {
    signIn: '/login'
  }
});

const middleware = (req: NextRequestWithAuth, event: NextFetchEvent) => {
  const publicPathnameRegex = RegExp(
    `^(/(${locales.join('|')}))?(${publicPages.join('|')})?/?$`,
    'i'
  );
  const isPublicPage = publicPathnameRegex.test(req.nextUrl.pathname);

  if (isPublicPage) {
    return intlMiddleware(req);
  } else {
    return authMiddleware(req, event);
  }
};

export default middleware;

export const config = {
  matcher: ['/((?!api|_next|.*\\..*).*)']
};
