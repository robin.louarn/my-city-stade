import { Messages } from '@/types';
import { getRequestConfig } from 'next-intl/server';
import { notFound } from 'next/navigation';

export const getMessages = async (locale: string): Promise<Messages> => {
  try {
    return (await import(`@/messages/${locale}`)).messages;
  } catch (error) {
    notFound();
  }
};

const intl = getRequestConfig(async ({ locale }) => ({
  messages: await getMessages(locale)
}));

export default intl;
