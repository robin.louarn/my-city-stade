'use client';

import { PushSubscription, subscribe, unsubscribe } from '@/lib/notification';
import { urlBase64ToUint8Array } from '@/lib/utils';
import {
  FC,
  PropsWithChildren,
  createContext,
  useContext,
  useEffect,
  useState
} from 'react';

export type ServiceWorkerProviderProps = PropsWithChildren;

export type ServiceWorkerProviderValue =
  | {
      state: 'instaled';
      subscribe: () => Promise<void>;
      unsubscribe: () => Promise<void>;
    }
  | {
      state: 'not_instaled';
    };

const defaultValue: ServiceWorkerProviderValue = {
  state: 'not_instaled'
};

const ServiceWorkerContext =
  createContext<ServiceWorkerProviderValue>(defaultValue);

export const ServiceWorkerProvider: FC<ServiceWorkerProviderProps> = ({
  children
}) => {
  const [serviceWorker, setServiceWorker] =
    useState<ServiceWorkerRegistration>();

  useEffect(() => {
    navigator.serviceWorker
      .register('/service-worker.js')
      .then(setServiceWorker);
  }, []);

  return (
    <ServiceWorkerContext.Provider
      value={
        serviceWorker
          ? {
              state: 'instaled',
              subscribe: async () => {
                const permission = await Notification.requestPermission();

                if (permission != 'granted') return;

                const subscription =
                  (await serviceWorker.pushManager.getSubscription()) ??
                  (await serviceWorker.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: urlBase64ToUint8Array(
                      process.env.NEXT_PUBLIC_VAPID_PUBLIC_KEY
                    )
                  }));

                await subscribe(subscription.toJSON() as PushSubscription);
              },
              unsubscribe: async () => {
                const subscription =
                  await serviceWorker.pushManager.getSubscription();

                if (subscription) {
                  await subscription.unsubscribe();
                }
                await unsubscribe();
              }
            }
          : defaultValue
      }
    >
      {children}
    </ServiceWorkerContext.Provider>
  );
};

export const useServiceWorker = () => {
  const context = useContext(ServiceWorkerContext);

  if (context === null)
    throw new Error('useServiceWorker is used ouside of ServiceWorkerProvider');

  return context;
};
