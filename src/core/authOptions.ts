import { db } from '@/db/prisma';
import { compare } from 'bcrypt';
import { NextAuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

export const authOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      credentials: {
        email: { type: 'email' },
        password: { type: 'password' }
      },
      authorize: async (credentials) => {
        const { email, password } = credentials ?? {};
        if (!email || !password) {
          throw new Error('Missing username or password');
        }
        const user = await db.user.findUnique({
          where: {
            email
          }
        });
        if (!user || !(await compare(password, user.password))) {
          return null;
        }
        return user;
      }
    })
  ],
  callbacks: {
    session: async ({ session, token: { sub } }) => {
      session.user.id = sub;
      return session;
    }
  }
};
