'use client';

import { Messages, PropsWithLocale } from '@/types';
import { Session } from 'next-auth';
import { SessionProvider } from 'next-auth/react';
import { NextIntlClientProvider } from 'next-intl';
import { ThemeProvider as NextThemesProvider } from 'next-themes';
import { FC, PropsWithChildren } from 'react';
import { ServiceWorkerProvider } from './ServiceWorkerProvider';

export type AppProviderProps = PropsWithChildren<
  PropsWithLocale<{ session: Session | null; messages: Messages }>
>;

export const AppProvider: FC<AppProviderProps> = ({
  children,
  session,
  messages,
  params: { locale }
}) => {
  return (
    <NextIntlClientProvider locale={locale} messages={messages}>
      <ServiceWorkerProvider>
        <NextThemesProvider
          attribute="class"
          defaultTheme="system"
          enableSystem
        >
          <SessionProvider session={session}>{children}</SessionProvider>
        </NextThemesProvider>
      </ServiceWorkerProvider>
    </NextIntlClientProvider>
  );
};
