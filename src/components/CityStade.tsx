import { PropsWithLocale } from '@/types';
import { BellRing } from 'lucide-react';
import { getTranslator } from 'next-intl/server';
import { FC } from 'react';
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle
} from './ui/card';
import { Switch } from './ui/switch';

export type CityStadeProps = PropsWithLocale<{
  name: string;
  address: string;
}>;

export const CityStade: FC<CityStadeProps> = async ({
  params: { locale },
  name,
  address
}) => {
  const t = await getTranslator(locale, 'cityStade');

  return (
    <Card className="w-[380px] m-5">
      <CardHeader>
        <CardTitle>{name}</CardTitle>
        <CardDescription>{address}</CardDescription>
      </CardHeader>
      <CardContent className="grid gap-4">
        <div className=" flex items-center space-x-4 rounded-md border p-4">
          <BellRing />
          <div className="flex-1 space-y-1">
            <p className="text-sm font-medium leading-none">{t('label')}</p>
            <p className="text-sm text-muted-foreground">{t('description')}</p>
          </div>
          <Switch />
        </div>
      </CardContent>
    </Card>
  );
};
