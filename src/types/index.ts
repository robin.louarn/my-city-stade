import { messages } from '@/messages/fr';
import { DefaultJWT } from 'next-auth/jwt';

export type PropsWithLocale<P = unknown> = P & { params: { locale: string } };

export type Messages = typeof messages;

declare global {
  interface IntlMessages extends Messages {}
  namespace NodeJS {
    interface ProcessEnv extends Record<string, string> {
      NEXTAUTH_SECRET: string;
      NEXT_PUBLIC_VAPID_PUBLIC_KEY: string;
      VAPID_PRIVATE_KEY: string;
    }
  }
}

declare module 'next-auth' {
  interface Session {
    user: {
      id: string;
      email: string;
    };
  }
}

declare module 'next-auth/jwt' {
  export interface JWT extends DefaultJWT {
    email: string;
    sub: string;
  }
}
