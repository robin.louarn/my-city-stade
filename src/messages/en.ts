export const messages = {
  // globale locales
  global: {
    login: 'Login',
    logout: 'Logout',
    register: 'Signup',
    home: 'Home',
    profile: 'Profile'
  },
  // pages
  homePage: { title: 'City' },
  loginPage: {
    email: 'Email',
    password: 'Password',
    error:
      '{error, select, CredentialsSignin {Email or password invalide} other {Unknow error}}'
  },
  registerPage: {
    email: 'Email',
    password: 'Password',
    firstname: 'Firstname',
    lastname: 'Lastname',
    error:
      '{error, select, CredentialsSignin {Email or password invalide} other {Unknow error}}'
  },
  profilePage: {},
  //components
  cityStade: {
    label: 'Notifications',
    description: 'Send notification to a device.'
  },
  modeToggle: {
    light: 'Light',
    dark: 'Dark',
    system: 'System'
  },
  localeSwitcher: {
    label: 'Swith Language',
    locale: '{locale, select, de {French} to {English} other {Unknown}}'
  },
  serviceWorkerSettings: {
    label: 'Enable notifications',
    button: 'Try the notif'
  }
} as const;
