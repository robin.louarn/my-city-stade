export const messages = {
  // globale locales
  global: {
    login: 'Se connecter',
    logout: 'Se déconnecter',
    register: "S'inscrire",
    home: 'Acceuil',
    profile: 'Profil'
  },
  // pages
  homePage: { title: 'City' },
  loginPage: {
    email: 'Email',
    password: 'Mot de passe',
    error:
      '{error, select, CredentialsSignin {Email ou mot de passe invalide} other {Error inconnue}}'
  },
  registerPage: {
    email: 'Email',
    password: 'Mot de passe',
    firstname: 'Prenom',
    lastname: 'Nom',
    error:
      '{error, select, CredentialsSignin {Email ou mot de passe invalide} other {Error inconnue}}'
  },
  profilePage: {},
  //components
  cityStade: {
    label: 'Notifications',
    description: 'Envoyer une notification au device.'
  },
  modeToggle: {
    light: 'Claire',
    dark: 'Sombre',
    system: 'Système'
  },
  localeSwitcher: {
    label: 'Change de langue',
    locale: '{locale, select, de {German} en {English} other {Unknown}}'
  },
  serviceWorkerSettings: {
    label: 'Activer les notification',
    button: 'Tester les notifs'
  }
} as const;
