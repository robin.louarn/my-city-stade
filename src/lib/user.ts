'use server';

import { db } from '@/db/prisma';
import { Prisma, User } from '@prisma/client';
import { hash } from 'bcrypt';

export const createUser = async ({
  password,
  ...inputs
}: Prisma.UserCreateInput): Promise<User> =>
  db.user.create({
    data: { ...inputs, password: await hash(password, 10) }
  });
