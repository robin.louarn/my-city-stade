import { defaultLocale } from '@/constants/intl';
import { cookies } from 'next/headers';

export const getLocale = () =>
  cookies().get('NEXT_LOCALE')?.value ?? defaultLocale;
