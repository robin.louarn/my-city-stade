'use server';

import { authOptions } from '@/core/authOptions';
import { getMessages } from '@/core/intl';
import { db } from '@/db/prisma';
import { Prisma } from '@prisma/client';
import { getServerSession } from 'next-auth';
import { createTranslator } from 'next-intl';
import * as webPush from 'web-push';
import { getLocale } from './locale';

export type PushSubscription = {
  endpoint: string;
  keys: {
    p256dh: string;
    auth: string;
  };
};

export const subscribe = async (pushSubscription: PushSubscription) => {
  const session = await getServerSession(authOptions);

  if (!session?.user) throw Error('Unautorised');

  await db.userPreferences.upsert({
    where: {
      userId: session.user.id
    },
    create: {
      userId: session.user.id,
      notificationEnable: true,
      pushSubscription
    },
    update: {
      notificationEnable: true,
      pushSubscription
    }
  });
};

export const unsubscribe = async () => {
  const session = await getServerSession(authOptions);

  if (!session?.user) throw Error('Unautorised');

  await db.userPreferences.update({
    where: {
      userId: session.user.id
    },
    data: {
      notificationEnable: false,
      pushSubscription: Prisma.JsonNull
    }
  });
};

webPush.setVapidDetails(
  'mailto:user@localhost.org',
  process.env.NEXT_PUBLIC_VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
);

export const createNotification = async (): Promise<
  webPush.SendResult | undefined
> => {
  const session = await getServerSession(authOptions);

  if (!session?.user) throw Error('Unautorised');

  const locale = getLocale();
  const messages = await getMessages(locale);

  const t = createTranslator({
    locale,
    messages,
    namespace: 'serviceWorkerSettings'
  });

  const { notificationEnable, pushSubscription } =
    await db.userPreferences.findUniqueOrThrow({
      where: {
        userId: session.user.id
      }
    });

  if (notificationEnable && pushSubscription) {
    return webPush.sendNotification(
      pushSubscription as PushSubscription,
      JSON.stringify({
        title: t('label'),
        message: t('button')
      })
    );
  }
};
