import { NavBar } from '@/app/[locale]/components/NavBar';
import { AppProvider } from '@/core/AppProvider';
import { authOptions } from '@/core/authOptions';
import { getMessages } from '@/core/intl';
import { getServerSession } from 'next-auth';
import { FC, PropsWithChildren } from 'react';

export type LocalParam = {
  params: {
    locale: string;
  };
};

export type LocaleLayoutProps = PropsWithChildren<LocalParam>;

const LocaleLayout: FC<LocaleLayoutProps> = async ({
  children,
  params: { locale }
}) => {
  const session = await getServerSession(authOptions);
  const messages = await getMessages(locale);

  return (
    <html lang={locale} suppressHydrationWarning>
      <head>
        <title>{messages.homePage.title}</title>
      </head>
      <body className="h-screen bg-background font-sans antialiased">
        <AppProvider params={{ locale }} messages={messages} session={session}>
          <NavBar />
          <main className="container h-full">{children}</main>
        </AppProvider>
      </body>
    </html>
  );
};

export default LocaleLayout;
