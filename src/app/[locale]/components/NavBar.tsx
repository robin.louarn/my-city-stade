'use client';

import { cn } from '@/lib/utils';
import { useSession } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { FC } from 'react';
import { buttonVariants } from '../../../components/ui/button';
import { LocaleSwitcher } from './LocaleSwitcher';
import { Logout } from './Logout';
import { ModeSwitcher } from './ModeSwitcher';

export const NavBar: FC = () => {
  const { status } = useSession();
  const pathname = usePathname();
  const t = useTranslations('global');

  const links = [
    {
      href: '/',
      label: t('home')
    },
    ...(status === 'authenticated'
      ? [
          {
            href: '/profile',
            label: t('profile')
          }
        ]
      : [])
  ];

  const authPaths = ['/login', '/register'];

  return (
    <nav className="flex items-center border-b justify-between h-16 p-5">
      <div className="flex px-4 space-x-4">
        {links.map(({ href, label }) => (
          <Link
            key={href}
            href={href}
            className={cn(
              'text-sm font-medium text-muted-foreground transition-colors hover:text-primary',
              {
                'text-primary': pathname === href
              }
            )}
          >
            {label}
          </Link>
        ))}
      </div>
      <div className="flex px-4 space-x-4">
        {status === 'authenticated' && <Logout>{t('logout')}</Logout>}
        {status === 'unauthenticated' && !authPaths.includes(pathname) && (
          <Link href="/login" className={buttonVariants({ variant: 'ghost' })}>
            {t('login')}
          </Link>
        )}
        <ModeSwitcher />
        <LocaleSwitcher />
      </div>
    </nav>
  );
};
