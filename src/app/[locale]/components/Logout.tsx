'use client';

import { signOut } from 'next-auth/react';
import { FC, PropsWithChildren } from 'react';
import { Button, ButtonProps } from '../../../components/ui/button';

export type LogoutProps = PropsWithChildren<Omit<ButtonProps, 'onClick'>>;

export const Logout: FC<LogoutProps> = ({ children, ...props }) => (
  <Button onClick={() => signOut()} {...props}>
    {children}
  </Button>
);
