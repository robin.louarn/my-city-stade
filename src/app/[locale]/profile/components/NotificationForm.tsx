'use client';

import { Button } from '@/components/ui/button';
import { Label } from '@/components/ui/label';
import { Switch } from '@/components/ui/switch';
import { useServiceWorker } from '@/core/ServiceWorkerProvider';
import { createNotification } from '@/lib/notification';
import { SwitchProps } from '@radix-ui/react-switch';
import { useTranslations } from 'next-intl';
import { FC } from 'react';

export type NotificationFormProps = SwitchProps;

export const NotificationForm: FC<NotificationFormProps> = ({
  defaultChecked
}) => {
  const sw = useServiceWorker();
  const t = useTranslations('serviceWorkerSettings');

  const onCheckedChange = async (checked: boolean) => {
    if (sw.state === 'not_instaled') return;
    if (checked) {
      await sw.subscribe();
    } else {
      await sw.unsubscribe();
    }
  };

  return (
    <div className="flex flex-col gap-5">
      <div className="flex flex-row items-center space-x-5">
        <Switch
          onCheckedChange={onCheckedChange}
          defaultChecked={defaultChecked}
        />
        <Label htmlFor="airplane-mode">{t('label')}</Label>
      </div>
      <div className="self-start">
        <Button onClick={async () => await createNotification()}>
          {t('button')}
        </Button>
      </div>
    </div>
  );
};
