import { NotificationForm } from '@/app/[locale]/profile/components/NotificationForm';
import { Label } from '@/components/ui/label';
import { authOptions } from '@/core/authOptions';
import { db } from '@/db/prisma';
import { getServerSession } from 'next-auth';
import { FC } from 'react';

const ProfilePage: FC = async () => {
  const session = await getServerSession(authOptions);

  if (session === null) return;

  const { email, preferences } = await db.user.findFirstOrThrow({
    where: { id: session.user.id },
    include: {
      preferences: true
    }
  });

  return (
    <section className="flex justify-center items-center h-[80vh]">
      <div className="flex flex-col gap-5 m-5">
        <Label>Email : {email}</Label>
        <NotificationForm defaultChecked={preferences?.notificationEnable} />
      </div>
    </section>
  );
};

export default ProfilePage;
