import { CityStade } from '@/components/CityStade';
import { db } from '@/db/prisma';
import { PropsWithLocale } from '@/types';
import { FC } from 'react';

export type ProfilePageProps = PropsWithLocale;

const ProfilePage: FC<ProfilePageProps> = async ({ params: { locale } }) => {
  const cityStades = await db.cityStade.findMany();

  return (
    <section>
      {cityStades.map(({ id, ...props }) => (
        <CityStade params={{ locale }} key={id} {...props} />
      ))}
    </section>
  );
};

export default ProfilePage;
