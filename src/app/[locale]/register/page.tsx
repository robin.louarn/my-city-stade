'use client';

import { Button, buttonVariants } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { createUser } from '@/lib/user';
import { signIn } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { FC, FormEvent, useState } from 'react';
import * as z from 'zod';

const registerSchema = z.object({
  username: z.string().min(2).max(50)
});

const RegisterPage: FC = () => {
  const t = useTranslations('registerPage');
  const tGlobal = useTranslations('global');
  const [error, setError] = useState<string>();
  const router = useRouter();

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (error) setError(undefined);

    const formData = new FormData(event.currentTarget);

    const user = {
      email: formData.get('email') as string,
      password: formData.get('password') as string,
      firstname: formData.get('firstname') as string,
      lastname: formData.get('lastname') as string
    };

    await createUser(user);

    const result = await signIn('credentials', {
      ...user,
      redirect: false
    });

    if (result?.error) {
      setError(result.error);
    } else {
      router.push('/');
    }
  };

  return (
    <section className="h-[800px] flex flex-col justify-center items-center space-y-10">
      <h1 className="text-2xl font-semibold tracking-tight">
        {tGlobal('register')}
      </h1>

      <form onSubmit={onSubmit} className="w-[300px] flex flex-col space-y-5">
        <Label htmlFor="email">{t('email')}</Label>
        <Input id="email" name="email" type="email" required />
        <Label htmlFor="password">{t('password')}</Label>
        <Input id="password" name="password" type="password" required />
        <Label htmlFor="firstname">{t('firstname')}</Label>
        <Input id="firstname" name="firstname" type="text" required />
        <Label htmlFor="lastname">{t('lastname')}</Label>
        <Input id="lastname" name="lastname" type="text" required />
        {error && <p>{t('error', { error })}</p>}
        <div className="flex flex-row space-x-2 justify-end">
          <Link href="/login" className={buttonVariants({ variant: 'ghost' })}>
            {tGlobal('login')}
          </Link>
          <Button type="submit"> {tGlobal('register')}</Button>
        </div>
      </form>
    </section>
  );
};

export default RegisterPage;
