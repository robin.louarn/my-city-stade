'use client';

import { Button, buttonVariants } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { signIn } from 'next-auth/react';
import { useTranslations } from 'next-intl';
import Link from 'next/link';
import { useRouter, useSearchParams } from 'next/navigation';
import { FC, FormEvent, useState } from 'react';

const LoginPage: FC = () => {
  const t = useTranslations('loginPage');
  const tGlobal = useTranslations('global');
  const [error, setError] = useState<string>();
  const router = useRouter();

  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get('callbackUrl') || '/';

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (error) setError(undefined);

    const formData = new FormData(event.currentTarget);

    const result = await signIn('credentials', {
      email: formData.get('email'),
      password: formData.get('password'),
      redirect: false
    });

    if (result?.error) {
      setError(result.error);
    } else {
      router.push(callbackUrl);
    }
  };

  return (
    <section className="h-[800px] flex flex-col justify-center items-center space-y-10">
      <h1 className="text-2xl font-semibold tracking-tight">
        {tGlobal('login')}
      </h1>
      {/** TODO*/}
      <form onSubmit={onSubmit} className="w-[300px] flex flex-col space-y-5">
        <Label htmlFor="email">{t('email')}</Label>
        <Input id="email" name="email" type="email" required />
        <Label htmlFor="password">{t('password')}</Label>
        <Input id="password" name="password" type="password" required />
        {error && <p>{t('error', { error })}</p>}
        <div className="flex flex-row space-x-2 justify-end">
          <Link
            href="/register"
            className={buttonVariants({ variant: 'ghost' })}
          >
            {tGlobal('register')}
          </Link>
          <Button type="submit">{tGlobal('login')}</Button>
        </div>
      </form>
    </section>
  );
};

export default LoginPage;
