import '@/styles/globals.css';
import { FC, PropsWithChildren } from 'react';

export const metadata = {
  title: 'My city stade'
};

export type RootLayoutProps = PropsWithChildren;

const RootLayout: FC<RootLayoutProps> = async ({ children }) => <>{children}</>;

export default RootLayout;
