const withNextIntl = require('next-intl/plugin')('./src/core/intl.ts');

/** @type {import('next').NextConfig} */
module.exports = withNextIntl({
  reactStrictMode: true,
  experimental: {
    serverActions: true
  }
});
