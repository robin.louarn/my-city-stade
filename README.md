# My city stage

## setup

```shell
# nvm
nvm use
# env
cp example.env.local .env
# pnpm
pnpm install
# prisma
pnpm db:push
pnpm db:generate
pnpm db:seed
# next
pnpm dev
```
