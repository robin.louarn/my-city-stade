module.exports = {
  semi: true,
  trailingComma: 'none',
  singleQuote: true,
  printWidth: 80,
  plugins: [
    require.resolve('prettier-plugin-organize-imports'),
    require.resolve('prettier-plugin-packagejson')
  ]
};
